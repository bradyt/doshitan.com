---
title: "GSoC 2012: Report 4"
meta-description: Halfway through GSoC and there's still plenty to do
tags: GNOME, GSoC
---

This report covers the fourth two week block of coding time, July 2 -- July
15. During that time, mid-term evaluations occurred and I'm happy to report
I passed.

# Week 7 #
Added a function in `iconGrid.js` to explicitly update its values from CSS to
work around the issue mentioned last time. Call it before we do calculations
in `searchDisplay.js` and it reports the right number from the beginning.
Consequently, the 'more' icon works correctly now.

# Week 8 #
Started implementing `app-drag-{begin,cancelled,end}` signals for app
launchers so we can differentiate them from the `item-drag-*` signals, which
they are currently lumped in with. This is necessary since we want to treat
app launchers differently than other items sometimes. For example, we want to
reveal the dash while searching only if an app launcher is dragged. See the
[design page][gnomeSearchDesign] for more info.

# Looking Forward #
I need to give the same treatment to the workspace thumnails as I am the dash.
This means splitting some things up since the thumbnails are currently tied to
the window previews (they are in the same layer). My goal is to have three top
level areas, in order: dash, content, workspace thumbnails. The dash and
workspace thumbnails are independent elements that are controlled as such and
in the middle is where we draw all the other stuff. But, having the workspace
thumbnails independent may not leave much room in the middle of the screen for
the search results/app drawer if the thumbnails space is always allocated and
it is just visually hidden. May have to experiment a bit here.

[gnomeSearchDesign]: https://live.gnome.org/GnomeShell/Design/Whiteboards/Search
