import           Control.Applicative ((<|>))
import           Data.List           (sortBy)
import qualified Data.Map            as M
import           Data.Maybe          (fromMaybe)
import           Data.Monoid         (mappend, mconcat)
import           Hakyll
import           System.FilePath     (takeBaseName)


import           Utils               (safeName)

import           Site.Config
import           Site.Fields
import           Site.Pandoc
import           Site.Routes

main :: IO ()
main = hakyllWith hakyllConf $ do
  tags <- buildTags ("posts/*" .&&. hasNoVersion) (fromCapture "tags/*/index.html" . safeName)
  let postTagsCtx = postCtx tags

  match "assets/img/*" $ do
    route   idRoute
    compile copyFileCompiler

  let jsFiles = ("assets/js/*.js" .||. fromList bootstrapPlugins)
                .&&. complement "assets/js/all.js"
                .&&. complement "assets/js/**/tests/**"
  match jsFiles $ compile getResourceBody

  jsDependency <- makePatternDependency jsFiles
  rulesExtraDependencies [jsDependency] $ create ["assets/js/all.js"] $ do
    route idRoute
    compile $ do
      files <- loadAll jsFiles
      let content = concatMap itemBody (sortJsFiles files)
      makeItem content >>= withItemBody (unixFilter "uglifyjs" [])

  match "assets/js/vendor/*" $ do
    route idRoute
    compile copyFileCompiler

  match "assets/less/**.less" $ compile getResourceBody

  lessDependency <- makePatternDependency "assets/less/**.less"
  rulesExtraDependencies [lessDependency] $ create ["assets/css/main.css"] $ do
    route idRoute
    compile $ loadBody "assets/less/include.less"
      >>= makeItem
      >>= withItemBody (unixFilter "lessc" ["-", "--compress", "-O2"])

  match ("pages/*" .&&. complement (fromList specialPages)) $ do
    route contentRoute
    compile $ mPandocCompiler
      >>= loadAndApplyTemplate "templates/page.html"    pageCtx
      >>= loadAndApplyTemplate "templates/default.html" defaultContext
      >>= relativizeUrls

  match ("posts/*" .&&. hasNoVersion) $ do
    route contentRoute
    compile $ mPandocCompiler
      >>= loadAndApplyTemplate "templates/post.html"    postTagsCtx
      >>= loadAndApplyTemplate "templates/default.html" postTagsCtx
      >>= relativizeUrls

  match "posts/*" $ version "feed" $ compile feedPandocCompiler

  match "projects/*" $ do
    route contentRoute
    compile $ do
      ident <- getUnderlying
      metadata <- getMetadata ident
      let projectTagName = fromMaybe (takeBaseName $ toFilePath ident) (lookupString "tag-name" metadata <|> lookupString "title" metadata)
      let getPostsForTag tag = fromList $ fromMaybe [] $ M.lookup tag $ M.fromList (tagsMap tags)
      projectTaggedPosts <- fmap (take 5) $ recentFirst =<< loadAll (getPostsForTag projectTagName)
      extraLinks <- return . map extraLinkToItem =<< getExtraLinks ident
      let projectPostFields = if null projectTaggedPosts
                                 then mempty
                                 else mconcat
                                        [
                                          listField "posts" postTagsCtx (return projectTaggedPosts)
                                        , field "all-posts-url" (return . return . (++) "/" . toFilePath $ tagsMakeId tags projectTagName)
                                        ]
      let projectCtx = mconcat
            [
              listField "extra-links" extraLinkCtx (return extraLinks)
            , projectPostFields
            , pageCtx
            ]

      mPandocCompiler
        >>= loadAndApplyTemplate "templates/project.html" projectCtx
        >>= loadAndApplyTemplate "templates/default.html" defaultContext
        >>= relativizeUrls

  match ("posts/*" .||. "pages/*") $ version "toc" $ compile tocPandocCompiler

  create ["log.html"] $ do
    route directorizeRoute
    listPosts "Log" "All posts on doshitan.com" "posts/*" tags

  match "pages/index.html" $ do
    route   $ gsubRoute "pages/" (const "")
    compile $ do
      posts <- fmap (take 5) . recentFirst =<< loadAll ("posts/*" .&&. hasNoVersion)
      let indexCtx = mconcat [ listField "posts" postTagsCtx (return posts)
                             , tagCloudField "tag-cloud" 85 165 tags
                             , defaultContext
                             ]

      getResourceBody
        >>= applyAsTemplate indexCtx
        >>= loadAndApplyTemplate "templates/default.html" indexCtx
        >>= relativizeUrls

  match "templates/*" $ compile templateCompiler

  tagsRules tags $ \tag pattern' -> do
    let title = "Posts tagged " ++ tag
    let description = "Posts tagged " ++ tag
    route idRoute
    listPosts title description pattern' tags

    -- Create Atom feed for each tag
    version "atom" $ do
      route $ setExtension "xml"
      compileFeed title pattern' tags

  create ["atom.xml"] $ do
    route idRoute
    compileFeed "All posts" "posts/*" tags

--------------------------------------------------------------------------------
listPosts :: String -> String -> Pattern -> Tags -> Rules ()
listPosts title description pattern' tags = compile $ do
      list <- recentFirst =<< loadAll (pattern' .&&. hasNoVersion)
      let ctx = mconcat [ constField "title" title
                        , constField "meta-description" description
                        , listField "posts" (postCtx tags) (return list)
                        , defaultContext
                        ]

      makeItem ""
        >>= loadAndApplyTemplate "templates/post-list.html" ctx
        >>= loadAndApplyTemplate "templates/page-title.html" ctx
        >>= loadAndApplyTemplate "templates/default.html" ctx
        >>= relativizeUrls


compileFeed :: String -> Pattern -> Tags -> Rules ()
compileFeed title pattern' tags = compile $ do
    let feedCtx = postCtx tags `mappend` bodyField "description"
    feedPattern <- getFeedVersionPattern pattern'
    loadAll feedPattern
      >>= fmap (take 10) . recentFirst
      >>= renderAtom (feedConf title) feedCtx


getFeedVersionPattern :: Pattern -> Compiler Pattern
getFeedVersionPattern pattern'= do
  normalItems <- loadAll pattern' :: Compiler [Item String]
  return $ fromList (map feedId normalItems)
  where
    feedId :: Item a -> Identifier
    feedId = setVersion (Just "feed") . itemIdentifier


sortJsFiles :: [Item a] -> [Item a]
sortJsFiles = sortBy sorter
  where
    sorter a _ =
      if itemIdentifier a == "assets/js/bootstrap/popover.js"
      then GT
      else EQ
