module Site.Routes where

import Hakyll
import System.FilePath.Posix (takeBaseName, takeDirectory, (</>))

contentRoute :: Routes
contentRoute = foldr1 composeRoutes [ directorizeRoute
                                    , gsubRoute "posts/" (const "")
                                    , gsubRoute "pages/" (const "")
                                    , gsubRoute "projects/" (const "")
                                    , stripDate
                                    ]

stripDate :: Routes
stripDate = gsubRoute "^[0-9]{4}-[0-9]{2}-[0-9]{2}-" (const "")

-- replace a foo/bar.md by foo/bar/index.html
-- this way the url looks like: foo/bar/ in most browsers
directorizeRoute :: Routes
directorizeRoute = customRoute createIndexRoute
  where
    createIndexRoute ident = takeDirectory p </> takeBaseName p </> "index.html"
                             where p = toFilePath ident
