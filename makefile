.PHONY: clean build build-prod deploy-prod distclean link-libs link-drafts sync-prod unlink-drafts version-assets

clean:
	cabal clean

build: link-drafts
	cabal run build

build-prod: unlink-drafts
	cabal run rebuild

deploy-prod: version-assets sync-prod

deploy-ci: version-assets sync-ci

distclean: clean
	cabal run clean

link-libs:
	ln -rsf libs/bootstrap/less assets/less/bootstrap
	ln -rsf libs/bootstrap/js assets/js/bootstrap

DRAFTS := $(shell find drafts -type f)

link-drafts:
	$(shell for i in $(DRAFTS); do ln -sf ../"$$i" posts/"$${i:7}"; done)

AWS_PROFILE ?= default
AWS_S3_BUCKET ?= some-bucket-name
SITE_DIR ?= _site

AWS_CLI_ARGS := s3 sync $(SITE_DIR) s3://$(AWS_S3_BUCKET) --delete

sync-prod:
	aws --profile $(AWS_PROFILE) $(AWS_CLI_ARGS)

sync-ci:
	aws $(AWS_CLI_ARGS) --size-only

unlink-drafts: link-drafts
	$(shell for i in $(DRAFTS); do rm posts/"$${i:7}"; done)

ASSETS := $(addprefix assets/, css/main.css js/all.js )

version-assets:
	for ASSET in $(ASSETS) ; do \
		HASH=$$(md5sum _site/$$ASSET | cut -c 1-5 ) ; \
		NEW_NAME=$$(dirname $$ASSET)/$$HASH.$$( echo $$ASSET | awk -F . '{print $$NF}' ) ; \
		sed -ri "s|$$(echo $$ASSET)|$$(echo $$NEW_NAME)|" $$(find _site/ -type f -iname '*.html') ; \
		mv _site/$$(echo $$ASSET) _site/$$(echo $$NEW_NAME) ; \
	done
