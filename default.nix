{ mkDerivation, base, bytestring, filepath, hakyll, lessc, pandoc
, pandoc-types, split, stdenv, uglify-js
}:

mkDerivation {
  pname = "doshitan-site";
  version = "0.0.0";
  src = ./.;
  isLibrary = false;
  isExecutable = true;
  buildDepends = [
    base bytestring filepath hakyll pandoc pandoc-types split
  ];
  buildTools = [ lessc uglify-js ];
  license = stdenv.lib.licenses.agpl3;
}
